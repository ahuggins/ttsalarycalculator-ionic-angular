// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('ttsalarycalculator', ['ionic','admobModule']);
var new_blog_posts = JSON.parse(localStorage.getItem('new_blog_posts')); //use json.parse to get boolean

app.run(function($ionicPlatform, $interval) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }


});
});

app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
 //routeprovider serves the different pieces of information
 //ionic uses ui router, which includes $stateProvider and $urlRouterProvider
  $stateProvider
    .state('tabs', {
      url: '/tab',
      abstact: true,
      templateUrl: 'templates/tabs.html',
      controller: 'TabCtrl'
    })
    .state('tabs.home', {
      cache: false,
      url: '/home',
      views:{
        'home-tab':{
          templateUrl: 'templates/calculator.html',
          controller: 'main'
        }
      }
    })
    .state('tabs.about', {
      url: '/about',
      views:{
        'about-tab':{
          templateUrl: 'templates/about.html',
        }
      }
    })
    .state('tabs.salary_yearly', {
      url: '/salary_yearly',
      views:{
        'home-tab':{
          templateUrl: 'templates/salary_yearly.html',
          controller: 'main'
        }
      }
    })
    .state('tabs.salary_monthly', {
      cache: false,
      url: '/salary_monthly',
      views:{
        'home-tab':{
          templateUrl: 'templates/salary_monthly.html',
          controller: 'main'
        }
      }
    })
    .state('tabs.salary_monthly5', {
      cache: false,
      url: '/salary_monthly5',
      views:{
        'home-tab':{
          templateUrl: 'templates/salary_monthly5.html',
          controller: 'main'
        }
      }
    })
    .state('tabs.salary_weekly', {
      url: '/salary_weekly',
      views:{
        'home-tab':{
        templateUrl: 'templates/salary_weekly.html',
        controller: 'main'
      }
    }
    })
    .state('tabs.salary_daily', {
      url: '/salary_daily',
      views:{
        'home-tab':{
        templateUrl: 'templates/salary_daily.html',
        controller: 'main'
      }
    }
    })
    .state('tabs.faqs', {
      url: '/faqs',
      views:{
        'about-tab':{
        templateUrl: 'templates/faq.html'
      }
    }
    })
    .state('tabs.blog', {
      cache:false,
      url: '/blog',
      views:{
        'blog-tab':{
        templateUrl: 'templates/blog.html',
        controller: 'BloggerCtrl'
      }
    }
    })
    .state('tabs.blog_post', {
      cache: false,
      url: '/blog_post/:post_id',
      views:{
        'blog-tab':{
        templateUrl: 'templates/blog_post.html',
        controller: 'BloggerCtrl'
      }
    }
    })

 
  $urlRouterProvider.otherwise("/tab/home");
 
});

app.factory('Input', function() {
 
  input = {};
  input={inputSalary:'',tax_year:{name:''},age_group:{name:''}}; 
  return input;
});



app.controller('main', function($scope, $ionicModal, Input, $state, admobSvc, $interval, $ionicPopup, BlogService){
 
  
   setTimeout(function(){
          admobSvc.createBannerView({ 
            publisherId: "ca-app-pub-6804428783975881/9718323259",
            isTesting:false
          });
        }, 50   * 1000);


  $scope.age_groups=
    [{name:'over 16 and under 60'},
    {name: 'over 60'}];

    $scope.tax_years=[{name:'2016'},{name:'2017'},{name:'2018'},{name:'2019'},{name:'2020'}];

    /*$scope.user.tax_year=$scope.tax_years[1];
    $scope.user.age_group=$scope.age_groups[0];*/

    $scope.user=Input;
    //initializes the tax_year and age_group on the view ---ng-init="user.age_group = user.age_group || age_groups[0]" -->
    

    $scope.goTo = function($period){
      if($period=="monthly")$state.go('tabs.salary_monthly');
      else if($period=="monthly5")$state.go('tabs.salary_monthly5');
      else if($period=="yearly")$state.go('tabs.salary_yearly');
      else if($period=="weekly")$state.go('tabs.salary_weekly');
      else if($period=="daily")$state.go('tabs.salary_daily');
      else if($period=="home")$state.go('tabs.home');
      else if($period=="blog")$state.go('tabs.blog');
    }

    $scope.reset = function(){
      $scope.user.inputSalary = '';
      $scope.user.td1 = '';
      $scope.user.tax_year=$scope.tax_years[$scope.tax_years.length-1];
      $scope.user.age_group=$scope.age_groups[0];
      $scope.showResults = false;
     // console.log("reset");
    }
      
    $scope.calculateSalary = function($period){
       //show new banner ad 50 seconds every view with this controller is selected
        /*setTimeout(function(){
          admobSvc.createBannerView({ 
            publisherId: "ca-app-pub-6804428783975881/9718323259",
            isTesting:true
          });
        }, 10   * 1000); */

        //show new intestitialAd 100 seconds after every view with this controller is selected
        /*setTimeout(function(){
          admobSvc.requestInterstitialAd({
            publisherId:          "ca-app-pub-6804428783975881/9718323259",
            interstitialAdId:     "ca-app-pub-6804428783975881/4509394453",
            autoShowInterstitial: true
        });
      }, 100   * 1000);*/

         
    if($scope.user.inputSalary<0) $scope.user.inputSalary=$scope.user.inputSalary*-1; //make it positive
    if(!$scope.user.td1) $scope.user.td1 = 0;

    $scope.nontaxable = function(){
      //https://oxfordbusinessgroup.com/overview/letter-law-comprehensive-review-tax-framework
      if($scope.user.tax_year.name>=2016 || $scope.user.age_group.name=="over 60") {return (72000+($scope.user.td1*12));}
      else if($scope.user.tax_year.name==2016){return (60000+($scope.user.td1*12));} 
    }


    //get nontaxable income
    $scope.gov_nontaxable = $scope.nontaxable();
    $scope.user.inputSalary=parseFloat($scope.user.inputSalary);

    /*-____________________GROSS INCOME_______________________*/
   $scope.grossIncome_yearly = (parseFloat($scope.user.inputSalary)*12).toFixed(2);
   $scope.hourlysalary = (parseFloat($scope.grossIncome_yearly)/2080).toFixed(2); //based on a 52_week and 40_hour month, you work 2080 hours per year
   $scope.grossIncome_monthly = parseFloat($scope.user.inputSalary).toFixed(2);
   $scope.grossIncome_weekly = (parseFloat($scope.user.inputSalary)*12/2080*40).toFixed(2);
   $scope.grossIncome_daily = (parseFloat($scope.user.inputSalary)*12/2080*8).toFixed(2);

    /*__________________________NIS_____________________________ 
    http://www.nibtt.net/Contribution_Rates/cont_Mar03_2014.htm 
   http://www.nibtt.net/NIBTT/Principles_NIS.html
   http://www.nibtt.net/Downloads/downloads_2012/Guide%20to%20Contributing%20to%20NIS.pdf
   persons under 16 and over 65 do not pay NIS
   persons between 60 - 65 who have retired and started collecting beneifits do not pay NIS
    http://www.nibtt.net/Downloads/downloas_2016/2016%20Rate%20Sheet.pdf tax rates from September 2016*/
   $scope.NIS_yearly = 0;
   $scope.NIS_monthly4 = 0;
   $scope.NIS_monthly5 = 0;
   $scope.NIS_weekly = 0;
   $scope.NIS_daily = 0;
   $scope.NIS_hourly = 0;



   $scope.calculateNIS = function(){
     if($scope.user.age_group.name=="over 60" || parseFloat($scope.grossIncome_weekly)<120){
      return 0;
     }
     else{
       if($scope.user.tax_year.name==2015 || $scope.user.tax_year.name==2016){ //assumes tax year is jan - dec
         switch(true){
          case ($scope.user.inputSalary>=780 && $scope.user.inputSalary<=1299.99 && $scope.user.age_group.name!="over 60"):
            return 9.60;
          
          case ($scope.user.inputSalary>=1300 && $scope.user.inputSalary<=1732.99 && $scope.user.age_group.name!="over 60"):
            return 14.00;
          
          
          case ($scope.user.inputSalary>=1733&& $scope.user.inputSalary<=2399.99 && $scope.user.age_group.name!="over 60"):
            return 18.80;
          
          case ($scope.user.inputSalary>=2400 && $scope.user.inputSalary<=2902.99 && $scope.user.age_group.name!="over 60"):
            return 24.20;
            
          case ($scope.user.inputSalary>=2903 && $scope.user.inputSalary<=3552.99 && $scope.user.age_group.name!="over 60"):
            return 29.80;
            
          case ($scope.user.inputSalary>=3553 && $scope.user.inputSalary<=4289.99 && $scope.user.age_group.name!="over 60"):
            return 36.20;
          
          case ($scope.user.inputSalary>=4290 && $scope.user.inputSalary<=4982.99 && $scope.user.age_group.name!="over 60"):
            return 42.80;
          
          case ($scope.user.inputSalary>=4983 && $scope.user.inputSalary<=5719.99 && $scope.user.age_group.name!="over 60"):
            return 49.40;
          
          case ($scope.user.inputSalary>=5720 && $scope.user.inputSalary<=6542.99 && $scope.user.age_group.name!="over 60"):
            return 56.60;
          
          case ($scope.user.inputSalary>=6543 && $scope.user.inputSalary<=7322.99 && $scope.user.age_group.name!="over 60"):
            return 64;
          
          case ($scope.user.inputSalary>=7323 && $scope.user.inputSalary<=8189.99 && $scope.user.age_group.name!="over 60"):
            return 71.60;
          
          case ($scope.user.inputSalary>=8190 && $scope.user.inputSalary<=9099.99 && $scope.user.age_group.name!="over 60"):
            return 79.80;
          
          case ($scope.user.inputSalary>=9100 && $scope.user.inputSalary<=10052.99 && $scope.user.age_group.name!="over 60"):
            return 88.40;
          
          case ($scope.user.inputSalary>=10053 && $scope.user.inputSalary<=11179.99 && $scope.user.age_group.name!="over 60"):
            return 98.00;
          
          case ($scope.user.inputSalary>=11180 && $scope.user.inputSalary<=11999.99 && $scope.user.age_group.name!="over 60"):
            return 107.00;
          
          case ($scope.user.inputSalary>=12000 && $scope.user.age_group.name!="over 60"):
            return 110.80;
          
          default:
            return 0.00;
         }
       }//endif
     else if($scope.user.tax_year.name>=2017){
      switch(true){
        case ($scope.user.inputSalary>=867 && $scope.user.inputSalary<=1472.99 && $scope.user.age_group.name!="over 60"):
          return 11.90;
        
        case ($scope.user.inputSalary>=1473 && $scope.user.inputSalary<=1949.99 && $scope.user.age_group.name!="over 60"):
          return 17.40;
        
        
        case ($scope.user.inputSalary>=1950&& $scope.user.inputSalary<=2642.99 && $scope.user.age_group.name!="over 60"):
          return 23.30;
        
        case ($scope.user.inputSalary>=2643 && $scope.user.inputSalary<=3292.99 && $scope.user.age_group.name!="over 60"):
          return 30.10;
          
        case ($scope.user.inputSalary>=3293 && $scope.user.inputSalary<=4029.99 && $scope.user.age_group.name!="over 60"):
          return 37.20;
          
        case ($scope.user.inputSalary>=4030 && $scope.user.inputSalary<=4852.99 && $scope.user.age_group.name!="over 60"):
          return 45.10;
        
        case ($scope.user.inputSalary>=4853 && $scope.user.inputSalary<=5632.99 && $scope.user.age_group.name!="over 60"):
          return 53.20;
        
        case ($scope.user.inputSalary>=5633 && $scope.user.inputSalary<=6456.99 && $scope.user.age_group.name!="over 60"):
          return 61.40;
        
        case ($scope.user.inputSalary>=6457 && $scope.user.inputSalary<=7409.99 && $scope.user.age_group.name!="over 60"):
          return 70.40;
        
        case ($scope.user.inputSalary>=7410 && $scope.user.inputSalary<=8276.99 && $scope.user.age_group.name!="over 60"):
          return 79.60;
        
        case ($scope.user.inputSalary>=8277 && $scope.user.inputSalary<=9272.99 && $scope.user.age_group.name!="over 60"):
          return 89.10;
        
        case ($scope.user.inputSalary>=9273 && $scope.user.inputSalary<=10312.99 && $scope.user.age_group.name!="over 60"):
          return 99.40;
        
        case ($scope.user.inputSalary>=10313 && $scope.user.inputSalary<=11396.99 && $scope.user.age_group.name!="over 60"):
          return 110.20;
        
        case ($scope.user.inputSalary>=11397 && $scope.user.inputSalary<=12652.99 && $scope.user.age_group.name!="over 60"):
          return 122.10;
        
        case ($scope.user.inputSalary>=12653 && $scope.user.inputSalary<=13599.99 && $scope.user.age_group.name!="over 60"):
          return 133.30;
        
        case ($scope.user.inputSalary>=13600 && $scope.user.age_group.name!="over 60"):
          return 138.10;
        
        default:
          return 0.00;
       }
     }//end else
     }
  }
  $scope.NIS_weekly = $scope.calculateNIS();
   
  $scope.NIS_yearly = parseFloat($scope.NIS_weekly * 52).toFixed(2);
  $scope.NIS_monthly5 = parseFloat($scope.NIS_weekly* 5).toFixed(2);
  $scope.NIS_monthly4 = parseFloat($scope.NIS_weekly* 4).toFixed(2);
  $scope.NIS_daily = parseFloat($scope.NIS_weekly/5).toFixed(2);
  $scope.NIS_hourly = parseFloat($scope.NIS_weekly/40).toFixed(2);
  $scope.NIS_weekly = $scope.NIS_weekly.toFixed(2);

  /*_____________________TAXABLE INCOME_______________________*/
   $scope.taxableIncome_yearly = (0).toFixed(2);
   $scope.taxableIncome_monthly = (0).toFixed(2);
   $scope.taxableIncome_weekly = (0).toFixed(2);
   $scope.taxableIncome_daily = (0).toFixed(2);
   $scope.taxableIncome_hourly = (0).toFixed(2);
   if($scope.grossIncome_yearly>$scope.gov_nontaxable) {
     $scope.taxableIncome_yearly= parseFloat($scope.grossIncome_yearly-$scope.gov_nontaxable-(.7*$scope.NIS_yearly)).toFixed(2);
     $scope.taxableIncome_monthly = parseFloat($scope.taxableIncome_yearly/12).toFixed(2);//grossIncome_monthly-5000;
     $scope.taxableIncome_weekly = parseFloat($scope.taxableIncome_yearly/52).toFixed(2);//taxableIncome_hourly*40;
     $scope.taxableIncome_daily = parseFloat($scope.taxableIncome_yearly/(52*5)).toFixed(2);//taxableIncome_hourly*8;
     $scope.taxableIncome_hourly = parseFloat($scope.taxableIncome_yearly/(52*5*8)).toFixed(2);//taxableIncome_yearly/2080;
    }

  /*_________________INCOME TAX/PAYE________________________
   http://www.ird.gov.tt/load_page.asp?ID=78 */

   paye=0.25
   if ($scope.user.inputSalary>1000000 && $scope.user.tax_year.name>=2017)
       paye = 0.30 //https://home.kpmg/xx/en/home/insights/2017/03/flash-alert-2017-054.html
    
   $scope.incomeTax_yearly = parseFloat(Math.ceil($scope.taxableIncome_yearly * paye)).toFixed(2);
   $scope.incomeTax_monthly = parseFloat(Math.ceil($scope.taxableIncome_monthly * paye)).toFixed(2);
   $scope.incomeTax_weekly = parseFloat(Math.ceil($scope.taxableIncome_weekly * paye)).toFixed(2);
   $scope.incomeTax_daily = parseFloat(Math.ceil($scope.taxableIncome_daily * paye)).toFixed(2);

   /*____________________HS_________________________________
    http://www.ird.gov.tt/load_page.asp?ID=103 
    persons under 16 and over 60 do not pay health surcharge
    employed persons whose monthly emoluments
are more than $469.99 or whose weekly
emoluments are more than $109.00â€”$8.25
per week. All other employed personsâ€”$4.80
per week;*/
   $scope.HS_yearly = 0;
   $scope.HS_monthly4 = 0;
   $scope.HS_monthly5 = 0;
   $scope.HS_weekly = 0;
   $scope.HS_daily = 0;
   $scope.HS_hourly = 0;
   
   if(parseFloat($scope.grossIncome_monthly)>469.99 && $scope.user.age_group.name!="over 60"){
    $scope.HS_weekly=8.25;
   }
   if (parseFloat($scope.grossIncome_monthly)<=469.99 &&  parseFloat($scope.grossIncome_monthly)!=0 && $scope.user.age_group.name!="over 60"){
    $scope.HS_weekly=4.80;
   }
   if (parseFloat($scope.grossIncome_monthly)<25.00 &&  parseFloat($scope.grossIncome_monthly)!=0 && $scope.user.age_group.name!="over 60"){
    $scope.HS_weekly=0;
   }
   if ((parseFloat($scope.grossIncome_monthly)<=0.00  && $scope.user.age_group.name!="over 60") || $scope.user.age_group.name=="over 60"){
    $scope.HS_weekly=0;
   }
   if($scope.user.age_group.name=="over 60"){
    $scope.HS_weekly=0;
   } 

   $scope.HS_hourly = parseFloat($scope.HS_weekly/40).toFixed(2);
   $scope.HS_daily = parseFloat($scope.HS_weekly/5).toFixed(2);
   $scope.HS_monthly4 = parseFloat($scope.HS_weekly* 4).toFixed(2);
   $scope.HS_monthly5 = parseFloat($scope.HS_weekly* 5).toFixed(2);
   $scope.HS_yearly = parseFloat($scope.HS_weekly * 52).toFixed(2);
   $scope.HS_weekly = $scope.HS_weekly.toFixed(2);

   /*-----------------TD1---------------------------------*/
   $scope.td1_yearly = parseFloat($scope.user.td1*12).toFixed(2);
   $scope.td1_monthly = parseFloat($scope.user.td1).toFixed(2);
   $scope.td1_weekly = parseFloat($scope.user.td1/4).toFixed(2);
   $scope.td1_daily = parseFloat($scope.user.td1/20).toFixed(2); //assuming 20 days in a month


   /*______________TAKE HOME SALARY_________________________*/
   $scope.takeHome_yearly = (parseFloat($scope.grossIncome_yearly) - ((parseFloat($scope.incomeTax_yearly))+parseFloat($scope.NIS_yearly)+parseFloat($scope.HS_yearly)+parseFloat($scope.td1_yearly))).toFixed(2);
   $scope.takeHome_monthly4 = (parseFloat($scope.grossIncome_monthly) - ((parseFloat($scope.incomeTax_monthly))+parseFloat($scope.NIS_monthly4)+parseFloat($scope.HS_monthly4)+parseFloat($scope.td1_monthly))).toFixed(2);
   $scope.takeHome_monthly5 = (parseFloat($scope.grossIncome_monthly) - ((parseFloat($scope.incomeTax_monthly))+parseFloat($scope.NIS_monthly5)+parseFloat($scope.HS_monthly5)+parseFloat($scope.td1_monthly))).toFixed(2);
   $scope.takeHome_weekly = (parseFloat($scope.grossIncome_weekly) - ((parseFloat($scope.incomeTax_weekly))+parseFloat($scope.NIS_weekly)+parseFloat($scope.HS_weekly)+parseFloat($scope.td1_weekly))).toFixed(2);
   $scope.takeHome_daily = (parseFloat($scope.grossIncome_daily) - ((parseFloat($scope.incomeTax_daily))+parseFloat($scope.NIS_daily)+parseFloat($scope.HS_daily)+parseFloat($scope.td1_daily))).toFixed(2);
  // console.log($scope.takeHome_yearly);
   } //end calculateSalary()

     // Triggered on a button click, or some other target
$scope.showPopup = function($state) {
  

  var myPopup = $ionicPopup.show({
    template: 'This calculation is based on a 4 week month. The monthly salary will be different for a month calculated with 5 weeks.',
    title: 'Monthly Salary',
    subTitle: 'Monthly Salary calculations are based on the number of weeks in the month.',
    scope: $scope,
    buttons: [
      { text: 'Ok' },
      {
        text: '<b>5 Week Month</b>',
        type: 'button-dark',
        onTap: function(e) {
          
            //don't allow the user to close unless he enters wifi password
            //e.preventDefault();
            $state.go('tabs.salary_monthly5');
          }
        }
      
    ]
  });


  $timeout(function() {
     myPopup.close(); //close the popup after 10 seconds for some reason
  }, 10000);
 }; //end showPopup

 /*$ionicModal.fromTemplateUrl('my-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });//end modal*/

  });

app.service('BlogService', ['$http', function($http){
    var blogPromise;
    var apiUrl = 'https://www.googleapis.com/blogger/v3/blogs/7149947100232736377';
    var apiKey = 'AIzaSyAny3_5S61Otb0aOTgRYmeeWtT5AxH5Tl8';

    this.getBlog = function(fetchBodies, fetchImages){
      if(fetchBodies===undefined) fetchBodies = true;
      if(fetchImages===undefined) fetchImages = true;
      if(!blogPromise || blogPromise.$$state.status!=1){//status 1 means we successfully retrieved data
          console.log(apiUrl+'/posts?key='+apiKey+'&fetchBodies='+fetchBodies+'&fetchImages='+fetchImages);
          blogPromise = $http.get(apiUrl+'/posts?key='+apiKey+'&fetchBodies='+fetchBodies+'&fetchImages='+fetchImages);
          /*$http.get(apiUrl+'/posts?key='+apiKey).then(function(data){
              blogPromise= data;
              console.log("blog promise in service" + blogPromise);
          });*/
      } //end if
      return blogPromise;
    } //end function


}]);


app.controller('BloggerCtrl', ['$scope','BlogService','$state','$stateParams','$ionicLoading', function($scope,BlogService, $state, $stateParams, $ionicLoading, $cordovaSocialSharing){
  console.log("last blog post index "+localStorage.getItem("last_blog_post_index"));

  $scope.posts = [];
  $scope.post_index=-1;
  $scope.blog_post=[];
  $ionicLoading.show();
  //$ionicLoading.show();
  //console.log("1response in controller"+ BlogService.getBlog());
  var max_post_id = localStorage.getItem("last_blog_post_index");
  BlogService.getBlog().then(function(response){
     console.log("response in controller"+response);

      //$ionicLoading.hide();
      
      response.data.items.forEach(function(val, key){
        $scope.posts.push(val);
        //alert(val.content.split(">"));

      });
      $scope.found = response.data.found;

      
      //console.log($scope.posts);
      // For JSON responses, resp.data contains the result
      $ionicLoading.hide();
      new_blog_posts=false;
      localStorage.setItem("new_blog_posts", new_blog_posts);
      $scope.blogTitle = 'Blog';
 
      
    
  }, 
  function(err) {
    $ionicLoading.hide();
    //alert('ERROR');
    //console.log(err)
     // err.status will contain the status code)
  });


  
  

  
  $scope.post_index = $stateParams.post_id; //get the post_index from stateParams;

  $scope.goToPost = function($index){

      $state.go('tabs.blog_post',{ "post_id": $index });
  }


  $scope.OtherShare=function($index){
      post_title = $scope.posts[$index].title
      post_url = "http://www.ttsalarycalc.com/#/tab/blog_post/"+$index;
      app_url = "https://play.google.com/store/apps/details?id=com.ionicframework.ttsalarycalculator144380&hl=en";
      post_subject = "Check out this post on TT Salary Calculator";
      file = null;
      window.plugins.socialsharing.share(post_subject, post_title, file, post_url);
    //$cordovaSocialSharing.shareVia
  }
}]);



app.controller('TabCtrl', ['$scope','BlogService','$state','$stateParams', '$interval', function($scope,BlogService, $state, $stateParams, $interval){
  //alert("new_blog_posts "+new_blog_posts);
  if(new_blog_posts===null || new_blog_posts===true){
    new_blog_posts=true;
    localStorage.setItem("new_blog_posts", new_blog_posts);

    $scope.blogTitle = 'Blog &#9679;';
  }else{
    $scope.blogTitle = 'Blog';
  }
  updateCss = $interval(function(){
    //if new_blog_posts is false then check the blogger api if there are new ones
    if(new_blog_posts===false){
      console.log(new_blog_posts===true);
      if(localStorage.getItem('last_blog_post_index')==null){
        localStorage.setItem('last_blog_post_index',0);
      }

    
      var max_post_id = localStorage.getItem("last_blog_post_index");
    
      BlogService.getBlog(false, false).then(function(response){
        
        response.data.items.forEach(function(val, key){
          console.log("val id "+val.id);
          if(val.id>max_post_id){
            max_post_id = val.id;
          }

        });

        if(max_post_id> localStorage.getItem("last_blog_post_index")){
              console.log("update max post id");
              new_blog_posts = true;
              localStorage.setItem("last_blog_post_index", max_post_id);
              localStorage.setItem("new_blog_posts", new_blog_posts);
            
        }else{
              console.log("no new posts");
        }

      

            
      });
      if(new_blog_posts===true){
        console.log("new blog posts true");
        $scope.blogTitle = 'Blog &#9679;';
      }
      else{
        console.log("new blog posts false");
        $scope.blogTitle='Blog';
      }
    }

  }, 10000);


}]);


